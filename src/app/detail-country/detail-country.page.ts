import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-detail-country',
  templateUrl: './detail-country.page.html',
  styleUrls: ['./detail-country.page.scss'],
})
export class DetailCountryPage implements OnInit {

  country: any;
  date: any;
  countryToSave: any;

  constructor(private router: Router, private route: ActivatedRoute, public afDB: AngularFireDatabase) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.country = this.router.getCurrentNavigation().extras.state;
      }
    });
  }

  addCountry(){
    this.countryToSave = {
      name: this.country.name,
      capital: this.country.capital,
      region: this.country.region,
      flag: this.returnUrlFlag(this.country.alpha2Code),
      date: this.date
    };

    this.afDB.list('Country/').push(
      this.countryToSave
    );
    this.router.navigate(['/home']);
  }

  dateProcess(event){
    this.date = event.detail.value;
  }

  returnUrlFlag(code){
    return 'http://www.geognos.com/api/en/countries/flag/' + code + '.png';
  }

  ngOnInit() {
  }
}
