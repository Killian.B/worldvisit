import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { NavigationExtras, Router } from '@angular/router';


@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.page.html',
  styleUrls: ['./add-country.page.scss'],
})
export class AddCountryPage implements OnInit {
  allCountry: any;

  constructor(private http: HttpClient, private router: Router) {
  }

  readAPI() {
    this.http.get('https://restcountries.eu/rest/v2/all')
      .subscribe((data) => {
        this.allCountry = data;
      });
  }

  showDetailCountry(country) {
    const navigationExtras: NavigationExtras = { state :  country};
    this.router.navigate(['/detail-country/'], navigationExtras);
  }

  ngOnInit() {
    this.readAPI();
  }

}
