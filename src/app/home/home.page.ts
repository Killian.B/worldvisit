import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import {Router} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public afDB: AngularFireDatabase, private router: Router) {
  }

  dataCountries = this.getCountries();

  ngOnInit() {
  }

  getCountries() {

    const myCountries = [];
    this.afDB.list('Country/').snapshotChanges(['child_added']).subscribe(countries => {
      countries.forEach(country => {
        myCountries.push( country.payload.exportVal());
      });
    });

    return myCountries;
  }

  returnUrlFlag(code){
    return 'http://www.geognos.com/api/en/countries/flag/' + code + '.png';
  }

  removeCountry(country){
    this.afDB.list('Country/').remove();
    window.location.reload();
  }
}
